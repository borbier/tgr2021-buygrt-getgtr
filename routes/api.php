<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('members', 'UserController@index');
// Route::get('members/{name}', 'UserController@show');

Route::post('login', 'AuthController@login');
Route::get('members', 'UserController@index');
Route::post('members', 'UserController@create');
Route::put('members', 'UserController@update');
Route::get('members/{email}', 'UserController@show');
Route::delete('members/{id}', 'UserController@delete');

Route::get('items', 'ItemController@index');
Route::post('items', 'ItemController@create');
Route::get('items/kind', 'ItemController@kind');
Route::get('items/list/{found}', 'ItemController@listByFound');
Route::get('items/list', 'ItemController@listTimeRange');
Route::get('items/timeframe', 'ItemController@sumTimeframe');

Route::post('status', 'RawStatusController@create');
Route::get('status', 'RawStatusController@list');
Route::get('status/current', 'RawStatusController@current');
