<?php

namespace App\Http\Controllers;

use App\RawStatus;
use App\SummaryStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RawStatusController extends Controller
{
    // Create raw status from Raspberry Pi
    public function create(Request $request) {
        $newStatus = new RawStatus;

        $newStatus->status = $request->input('status');
        $newStatus->created_at = Carbon::now('Asia/Bangkok');

        $created = $newStatus->save();

        if ($created) {
            return response()->json([
                'created' => $created,
            ], 201);
        } else {
            return response()->json([
                'created' => $created,
                'message' => 'Item creation errored',
            ], 500);
        }
    }

    // List all raw status
    public function list() {
        return SummaryStatus::all();
    }

    public function current() {
        $refTimestampStringStart = Carbon::now('Asia/Bangkok')->format('Y-m-d H:i');
        $refTimestamp = Carbon::parse($refTimestampStringStart);
        $refTimestampEnd = Carbon::parse($refTimestampStringStart)->addMinute();

        $count = DB::table('raw_statuses')->whereBetween('created_at', [$refTimestamp, $refTimestampEnd])
            ->count();

        return response()->json([
            'refTimestamp' => $refTimestamp,
            'endRef' => $refTimestampEnd,
            'count' => $count,
        ], 200);
    }
}
