<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    // Get all member
    public function index() {
        return User::orderBy('created_at', 'DESC')->take(10)->get();
    }

    // Get specific member by name
    public function show($email) {
        $user = DB::table('users')
            ->select('id', 'name', 'surname', 'email')
            ->where('email', $email)->get();

        if (!empty($user)) {
            return response()->json($user, 200);
        } else {
            return response()->json([
                'message' => 'Not found',
            ], 404);
        }
    }

    // Update user information
    public function update(Request $request) {
        $id = $request->input('id');

        $user = User::find($id);

        if (!empty($user)) {
            if ($request->input('name')) {
                $user->name = $request->input('name');
            }
    
            if ($request->input('surname')) {
                $user->surname = $request->input('surname');
            }

            $user->save();

            return response()->json([
                'message' => 'Updated',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Not found',
            ], 404);
        }
    }

    // Create a user
    public function create(Request $request) {
        $result = DB::table('users')->insert(([
            'name' => $request->input('name'),
            'surname' => $request->input('surname'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]));

        if ($result) {
            return response()->json([
                'message' => 'User created',
            ], 201);
        } else {
            return response()->json([
                'message' => 'Error occured during user creation.',
            ], 500);
        }
    }

    // Delete user
    public function delete($id) {
        $user = User::find($id);
        $result = $user->delete();

        if ($result) {
            return response()->json([
                'message' => 'User deleted. RIP.',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Error occured during user deletion.',
            ], 500);
        }
    }
}
