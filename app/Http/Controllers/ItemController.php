<?php

namespace App\Http\Controllers;

use App\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Phattarachai\LineNotify\Facade\Line;

class ItemController extends Controller {
    // Get all items
    public function index() {
        return Item::all();
    }

    // Create an item
    public function create(Request $request) {
        $allowed = array('gif', 'png', 'jpg');
        
        if ($request->hasFile('img')) {
            $file = $request->file('img');

            $path = public_path() . '/uploads/images/';
            $file->move($path, 'payload.jpg');

            Line::imagePath($path . 'payload.jpg')
                ->send('ตรวจพบสินค้ามีตำหนิ');
        }

        $newItem = new Item;

        $newItem->found = $request->input('found');
        $newItem->size = $request->input('size');
        $newItem->created_at = Carbon::now()->timezone('Asia/Bangkok');

        $created = $newItem->save();

        if ($created) {
            return response()->json([
                'created' => $created,
            ], 201);
        } else {
            return response()->json([
                'created' => $created,
                'message' => 'Item creation errored',
            ], 500);
        }
    }

    // Get list of unique item
    public function kind() {
        $listOfKinds = Item::groupBy('found')
            ->selectRaw('sum(quantity) as sum, found')
            ->get();

        return response()->json($listOfKinds, 200);
    }

    // Get list of items by item.found
    public function listByFound($found) {
        $itemList = DB::table('items')
            ->where('found', $found)
            ->get();

        return response()->json($itemList, 200);
    }

    // // Get item counts by item.found
    public function listTimeRange(Request $request) {
        $from = $request->query('from');
        $to = $request->query('to');
        $found = $request->query('found');

        if ($found == 'all') {
            $itemList = DB::table('items')
                ->whereBetween('created_at', [$from, $to])
                ->orderBy('created_at', 'ASC')
                ->get();
    
            return response()->json($itemList, 200);
        } else if (!isset($from) or !isset($to)) {
            $itemList = DB::table('items')
                ->where('found', '=', $found)
                ->orderBy('created_at', 'ASC')
                ->get();
    
            return response()->json($itemList, 200);
        } else {
            $itemList = DB::table('items')
                ->whereBetween('created_at', [$from, $to])
                ->where('found', '=', $found)
                ->orderBy('created_at', 'ASC')
                ->get();
    
            return response()->json($itemList, 200);
        }
    }

    // Query non lime
    public function listNonLime() {
        $itemList = DB::table('items')
            ->where('found', '!=', 'Lime')
            ->orderBy('created_at', 'ASC')
            ->get();

        return response()->json($itemList, 200);
    }

    // Query sum with timeframe
    public function sumTimeframe(Request $request) {
        $size = $request->input('size');
        $found = $request->input('found');

        if (isset($size)) {
            $itemList = DB::table('items')
                ->select(DB::raw('DATE_FORMAT(created_at, \'%d/%m/%Y %H:00\') as timestamp, COUNT(*) as quantity'))
                ->where('size', '=', $size)
                ->where('found', '=', $found)
                ->groupBy('timestamp')
                ->get();
        
            return response()->json($itemList, 200);
        } else {
            $itemList = DB::table('items')
                ->select(DB::raw('DATE_FORMAT(created_at, \'%d/%m/%Y %H:00\') as timestamp, COUNT(*) as quantity'))
                ->where('found', '=', $found)
                ->groupBy('timestamp')
                ->get();
        
            return response()->json($itemList, 200);
        }

    }
}
