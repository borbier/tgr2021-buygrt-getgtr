<?php

namespace App\Console\Commands;

use App\SummaryStatus;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CompileStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $refTimestampStringStart = Carbon::now('Asia/Bangkok')->subMinute()->format('Y-m-d H:i');
        $refTimestamp = Carbon::parse($refTimestampStringStart);
        $refTimestampEnd = Carbon::parse($refTimestampStringStart)->addMinute();

        $count = DB::table('raw_statuses')->whereBetween('created_at', [$refTimestamp, $refTimestampEnd])
            ->count();
        
        DB::table('summary_statuses')
            ->insert([
                'count' => $count,
                'created_at' => $refTimestamp,
            ]);

        info($refTimestamp);
    }
}
