<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'L',
            'created_at' => Carbon::create(2021, 02, 23, 16, 0, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'L',
            'created_at' => Carbon::create(2021, 02, 23, 16, 5, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'S',
            'created_at' => Carbon::create(2021, 02, 23, 16, 7, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'L',
            'created_at' => Carbon::create(2021, 02, 23, 16, 9, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'S',
            'created_at' => Carbon::create(2021, 02, 23, 16, 31, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Marker',
            'size' => 'S',
            'created_at' => Carbon::create(2021, 02, 23, 17, 16, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'S',
            'created_at' => Carbon::create(2021, 02, 23, 17, 22, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'L',
            'created_at' => Carbon::create(2021, 02, 23, 17, 31, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'L',
            'created_at' => Carbon::create(2021, 02, 23, 17, 35, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'L',
            'created_at' => Carbon::create(2021, 02, 23, 17, 40, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'L',
            'created_at' => Carbon::create(2021, 02, 23, 18, 6, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Lime',
            'size' => 'S',
            'created_at' => Carbon::create(2021, 02, 23, 18, 11, 0, 'Asia/Bangkok'),
        ]));
        DB::table('items')->insert(([
            'found' => 'Marker',
            'size' => 'S',
            'created_at' => Carbon::create(2021, 02, 23, 18, 21, 0, 'Asia/Bangkok'),
        ]));
    }
}
