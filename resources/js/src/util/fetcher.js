import axios, { AxiosRequestConfig } from "axios";

export const fetch = (url, config) => {
  return axios.get(url, config)
    .then((res) => {
      return res.data
    })
    .catch((err) => {
      console.log(err);
    })
}
