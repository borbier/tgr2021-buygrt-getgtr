import React, { useState, useEffect } from 'react';
import Navbar from '../components/general/Navbar';
import LiveFeed from '../components/LiveFeed';
import MachineStatus from '../components/MachineStatus';
import TimeframeGraph from '../components/TimeframeGraph';

import { getCurrentUser } from '../services/users.service';

function Dashboard() {
    const [username, setUsername] = useState("");

    useEffect(() => {
        const _username = getCurrentUser();
        setUsername(_username);
    }, []);

    console.log(username);
    
    return (
        <div className="w-screen">
            <Navbar 
                onLogin={() => {
                    const _username = getCurrentUser();
                    setUsername(_username);
                }}
            />
            {
                (username !== "") ?
                <div>
                    <MachineStatus />
                    <TimeframeGraph />
                    <LiveFeed />
                </div>
                :
                <div className="flex items-center justify-center h-screen w-full ">
                    <div>
                        🔒 Login before seeing this page
                    </div>
                </div>
            }
        </div>
    );
}

export default Dashboard;