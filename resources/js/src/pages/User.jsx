import React from 'react';
import Navbar from '../components/general/Navbar';
import Profile from '../components/Profile';
import UserList from '../components/UserList';

function User() {
    return (
        <div className="w-screen">
            <Navbar 
                onLogin={() => console.log("Ping")}
            />
            <Profile />
            <UserList />
        </div>
    )
}

export default User;