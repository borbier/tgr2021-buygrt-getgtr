// import useSWR from 'swr';
// import { fetch } from '../util/fetcher';

import Axios from "axios"
import useSWR from "swr";
import { fetch } from "../util/fetcher";

export const login = async ({ email, password }) => {
    const response = await Axios.post('/api/login', {
        email: email,
        password: password
    }).then((res) => res);

    return response;
}

export const getCurrentUser = () => {
    const token = localStorage.getItem('auth-tgr2021');
    if (token) {
        return localStorage.getItem('auth-email');
    }

    return '';
}

export const getUserInfo = (email) => {
    const { data, error, mutate } = useSWR(`/api/members/${email}`, fetch);

    return {
        data: data,
        loading: !data && !error,
        error: error,
        mutate: mutate,
    }
}

export const getUserList= () => {
    const { data, error, mutate } = useSWR(`/api/members`, fetch);

    return {
        data: data,
        loading: !data && !error,
        error: error,
        mutate: mutate,
    }
}

export const createUser = async (body) => {
    const result = await Axios.post('/api/members', body)
        .then(res => res);
    
    return result;
}

export const updateUser = async (body) => {
    const result = await Axios.put('/api/members', body)
        .then(res => res);
    
    return result;
}

export const deleteUser = async (id) => {
    const result = await Axios.delete(`/api/members/${id}`)
        .then(res => res);

    return result;
}

export const logout = () => {
    localStorage.removeItem('auth-tgr2021');
    localStorage.removeItem('auth-email');
}