import useSWR from 'swr';
import { fetch } from '../util/fetcher';

export const getLivefeed = () => {
    const { data, error } = useSWR('api/items', fetch);

    return {
        data: data,
        loading: !data && !error,
        error: error,
    };
}

export const getTimeframe = (found, size) => {
    let url = '';
    if (size) {
        url = `api/items/timeframe?found=${found}&size=${size}`;
    } else {
        url = `api/items/timeframe?found=${found}`;
    }
    
    const { data, error } = useSWR(url, fetch);

    return {
        data: data,
        loading: !data && !error,
        error: error,
    };
}
