import useSWR from 'swr';
import { fetch } from '../util/fetcher';

export const currentStatus = () => {
    const { data, error } = useSWR('api/status/current', fetch);

    return {
        data: data,
        loading: !data && !error,
        error: error,
    };
}
