import React, { useState, useEffect } from "react";
import Avatar from "react-avatar";
import { getCurrentUser, getUserInfo } from "../services/users.service";
import UpdateProfileModal from "./modals/UpdateProfileModal";

function Profile() {
    const [email, setEmail] = useState("");
    const [updateProfile, setUpdateProfile] = useState(false);
    const { data, loading, mutate } = getUserInfo(email);

    useEffect(() => {
        const _username = getCurrentUser();
        setEmail(_username);
    }, [data]);

    console.log(data);
    return (
        <>
            <div className="bg-white p-4 m-4 shadow-lg">
            <div className="flex flex-row justify-between items-center">
                <div className="text-lg">
                    Profile
                </div>
                <button 
                    className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
                    onClick={() => setUpdateProfile(!updateProfile)}
                >
                    Update Profile
                </button>
            </div>
            {
                !loading &&
                <div className="flex flex-row">
                    <div className="pr-4">
                        <Avatar name={`${data[0].name} ${data[0].surname}`} />
                    </div>
                    <ul>
                        <li>Name : {data[0].name} {data[0].surname}</li>
                        <li>Email : {data[0].email}</li>
                    </ul>
                </div>
            }
            </div>
            {
                (updateProfile && !loading) && <UpdateProfileModal 
                    id={data[0].id}
                    name={data[0].name}
                    surname={data[0].surname}
                    onClose={() => {
                        mutate();
                        setUpdateProfile(!updateProfile);
                    }}
                />
            }
        </>
    )
}

export default Profile;
