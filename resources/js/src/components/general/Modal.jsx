import React from "react";
import { MdClose } from "react-icons/md";

const ModalContainer = ({
    children,
    handleClose,
    heading,
  }) => {
    return (
      <div className="flex justify-center items-center fixed z-10 left-0 top-0 w-screen h-screen overflow-auto bg-gray-500 bg-opacity-50">
        <div className="flex flex-col bg-white p-2 w-4/5 shadow-md rounded">
          <div className="flex justify-between items-center">
            <div className="text-xl">{heading}</div>
            <MdClose onClick={() => handleClose()} />
          </div>
          {children}
        </div>
      </div>
    );
};

export default ModalContainer;