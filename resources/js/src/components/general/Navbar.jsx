import React, { useState, useEffect } from "react";
import { getCurrentUser, logout } from "../../services/users.service";
import LoginModal from "../modals/LoginModal";

function Navbar({ onLogin }) {
    const [login, setLogin] = useState(false);
    const [username, setUsername] = useState("");

    async function handleLogout() {
        logout();
        setUsername("");
    }

    useEffect(() => {
        const _username = getCurrentUser();
        setUsername(_username);
        onLogin(login);
    }, [login]);

    return (
        <>
            <div className="flex flex-row justify-between items-center bg-blue-500 w-full text-lg p-2 px-4 text-white shadow-lg">
                <div>TGR Factory ของคนไม่ได้นอน</div>
                <div>
                    {
                        username !== "" ?
                        <div>
                            <span>{username}</span>
                            <button 
                                className="hover:bg-blue-700 text-white py-2 px-4 rounded"
                                onClick={() => {
                                    onLogin(false);
                                    handleLogout();
                                }}
                            >
                                Logout
                            </button>
                        </div>
                        :
                        <button 
                            className="hover:bg-blue-700 text-white py-2 px-4 rounded"
                            onClick={() => {
                                
                                setLogin(!login);
                            }}>
                            Login
                        </button>
                    }
                </div>
            </div>
            {
                login && <LoginModal 
                    onClose={() => {
                        setLogin(!login);
                    }}
                />
            }
        </>
    );
}

export default Navbar;