import React, { useEffect, useState } from 'react';

function Table({ data }) {
    const [total, setTotal] = useState(0);

    useEffect(() => {
        setTotal(data.length);
    }, [data]);

    return (
        <div>
            <table className="table-fixed w-full">
                <thead>
                    <tr>
                        <th className="px-2 w-1/5">Id</th>
                        <th className="px-2 w-1/5">Item</th>
                        <th className="px-2 w-1/5">Size</th>
                        <th className="px-2 w-2/5">Timestamp</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map((value) => {
                            return (
                                <tr>
                                    <td className="border px-2">{value.id}</td>
                                    <td className="border px-2">{value.found}</td>
                                    <th className="border px-2">{value.size}</th>
                                    <td className="border px-2">{new Date(value.created_at).toLocaleString()}</td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default Table;
