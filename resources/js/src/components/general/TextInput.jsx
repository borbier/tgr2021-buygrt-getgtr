import React from "react";

const TextInput = ({ name, type,  label, placeholder, handleChange, value }) => {
    return (
      <div className="my-2">
        <label
          className='block text-gray-700 text-sm font-bold mb-2'
          htmlFor='action'
        >
          {label}
              </label>
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
          type={type}
          name={name}
          onChange={handleChange}
          value={value}
          placeholder={placeholder || ''}
        />
      </div>
    );
  }
  
  export default TextInput;
