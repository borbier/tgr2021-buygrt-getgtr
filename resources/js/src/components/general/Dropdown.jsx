import React, { useState } from 'react';

function TDropdown ({ options, onOptionSelect }) {
    const [isActive, setActive] = useState(false);
    const [selected, setSelected] = useState("Options");
    
    const enableOption = isActive ? 'block' : 'hidden'
    return (
      <div className="w-full border rounded p-2">
        <button type="button" className="w-full" onClick={() => setActive(!isActive)}>
          {selected}
        </button>
        <div
            className={`origin-top-right absolute mt-2 p-2 w-56 bg-white rounded-md shadow-lg ${enableOption} z-10`}
        >
          {options.map((option) => 
            <div 
              key={option.value} 
              onClick={() => {
                onOptionSelect(option.value);
                setSelected(option.name || option.value);
                setActive(!isActive);
              }}
            >
              {option.name}
            </div>
          )}
        </div>
      </div>
    );
}

export default TDropdown;
