import Axios from 'axios';
import React from 'react';
import { getLivefeed } from '../services/items.service';
import Table from './general/Table';

function LiveFeed() {
    const { data, loading } = getLivefeed();
        
    return (
        <div className="bg-white p-4 m-4 shadow-lg">
            <div className="text-2xl">Live feed</div>
            <hr />
            {
                !loading && <div className="pt-4">
                    <Table data={data} />
                </div>
            }
        </div>
    );
}

export default LiveFeed;
