import React, { useState } from 'react';
import { getTimeframe } from '../services/items.service';
import TDropdown from './general/Dropdown';
import { AreaChart, ResponsiveContainer, CartesianGrid, XAxis, YAxis, Tooltip, Area } from 'recharts';

const sizeOptions = [
    { name: "All", value: "" },
    { name: "Small", value: "S" },
    { name: "Large", value: "L" },
];

const typeOptions = [
    { name: "Lime", value: "Lime" },
    { name: "Marker", value: "Marker" },
];

function TimeframeGraph() {
    const [size, setSize] = useState("S");
    const [found, setFound] = useState("Lime");
    const { data, loading } = getTimeframe(
        found,
        size
    );

    return (
        <div className="bg-white p-4 m-4 shadow-lg">
            <div className="text-2xl">Timeframe Data</div>
            <hr />
            <div className="flex flex-row">
                <div>
                    <div>Type</div>
                    <span>
                    <TDropdown 
                        options={typeOptions}
                        onOptionSelect={(value) => setSize(value)}
                    />
                    </span>
                </div>
                <div>
                    <div>Size</div>
                    <span>
                    <TDropdown 
                        options={sizeOptions}
                        onOptionSelect={(value) => setSize(value)}
                    />
                    </span>
                </div>
            </div>
            <div className="h-64">
                <ResponsiveContainer width="100%" height="100%">
                <AreaChart
                    width={500}
                    height={400}
                    data={data}
                    margin={{
                        top: 10,
                        right: 30,
                        left: 0,
                        bottom: 0,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="timestamp" />
                    <YAxis />
                    <Tooltip />
                    <Area type="monotone" dataKey="quantity" stroke="#8884d8" fill="#8884d8" />
                </AreaChart>
                </ResponsiveContainer>
            </div>
        </div>
    )
}

export default TimeframeGraph;
