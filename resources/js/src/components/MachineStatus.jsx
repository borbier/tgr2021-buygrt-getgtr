import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { currentStatus } from '../services/status.service';

function MachineStatus() {
    const { data, loading } = currentStatus();
    const [status, setStatus] = useState("Loading");

    function renderStatus() {
        if (!loading) {
            if (data.count > 0) {
                return "Online";
            } else {
                return "Offline";
            }
        }
        return "Loading"
    }

    function renderTimestamp() {
        if (!loading) {
            return new Date(data.refTimestamp).toLocaleString('th-TH')
        }
        return "-";
    }

    useEffect(() => {
        const _status = renderStatus();
        setStatus(_status);
    }, [data]);

    return (
        <div className="flex flex-row justify-end">
            <div
                className={classNames("p-4 m-4 shadow-lg", {
                    'bg-gray-400': status === "Offline" || status === "Loading",
                    'bg-green-500 text-white': status === "Online",
                    'bg-red-500 text-white': status === "Defect"
                })}
            >
                <div className="text-sm">{status}</div>
                <div className="text-xs">Latest update : {renderTimestamp()}</div>
            </div>
        </div>
    );
}

export default MachineStatus;
