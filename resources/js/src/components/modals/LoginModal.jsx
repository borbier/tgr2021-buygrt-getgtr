import { Formik } from "formik";
import React, { useState } from "react";
import { login } from "../../services/users.service";
import ModalContainer from "../general/Modal";
import TextInput from "../general/TextInput";

function LoginModal({ onClose }) {
    const [error, setError] = useState(false);

    return (
        <ModalContainer
            heading="Login"
            handleClose={onClose}
        >
            <Formik
                initialValues={{
                    email: "",
                    password: "",
                }}
                onSubmit={async (values) => {
                    const response = await login(values);

                    if (response.status === 400) {
                        setError(true);
                    }

                    if (response.status === 200) {
                        localStorage.setItem('auth-tgr2021', response.data.token);
                        localStorage.setItem('auth-email', values.email);

                        onClose();
                    }
                }}
            >
                {({ values, handleChange, handleSubmit }) => (
                    <div className="flex flex-col">
                        <TextInput 
                            name="email"
                            type="email"
                            label="Email"
                            handleChange={handleChange}
                            value={values.email}
                        />
                        <TextInput 
                            name="password"
                            type="password"
                            label="Password"
                            handleChange={handleChange}
                            value={values.password}
                        />
                        {
                            error && <div className="text-red">Email or password is incorrect.</div>
                        }
                        <button 
                            className="w-full bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
                            onClick={handleSubmit}
                        >
                            Sign In
                        </button>
                    </div>
                )}
            </Formik>
        </ModalContainer>
    );
}

export default LoginModal;