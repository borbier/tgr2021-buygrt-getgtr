import React from "react";
import { deleteUser } from "../../services/users.service";
import ModalContainer from "../general/Modal";

function DeleteUserModal({ id, name, onClose }) {
    return (
        <ModalContainer
            heading="Delete User"
            handleClose={onClose}
        >
            <div>
                Are you sure deleting user "{name}" ?
            </div>
            <button 
                className="w-full bg-red-500 hover:bg-red-700 text-white py-2 px-4 rounded"
                onClick={async () => {
                    const response = await deleteUser(id);

                    if (response.status === 200) {
                        onClose();
                    }
                }}
            >
                Delete user
            </button>
            <button 
                className="w-full bg-gray-500 hover:bg-gray-700 text-white py-2 px-4 rounded"
                onClick={async () => {
                    onClose();
                }}
            >
                Cancel
            </button>
        </ModalContainer>
    );
}

export default DeleteUserModal;
