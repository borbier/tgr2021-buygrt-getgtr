import { Formik } from "formik";
import React from "react";
import { updateUser } from "../../services/users.service";
import ModalContainer from "../general/Modal";
import TextInput from "../general/TextInput";

function UpdateProfileModal({ id, name, surname, onClose }) {
    return (
        <ModalContainer
            heading="Update Profile"
            handleClose={onClose}
        >
            <Formik
                initialValues={{
                    name: "",
                    surname: "",
                }}
                onSubmit={async (values) => {
                    const response = await updateUser({id, ...values});
                    
                    if (response.status === 200) {
                        onClose();
                    }
                }}
            >
                {({ values, handleChange, handleSubmit }) => (
                    <div className="flex flex-col">
                        <TextInput 
                            name="name"
                            type="text"
                            label="Name"
                            handleChange={handleChange}
                            value={values.name}
                        />
                        <TextInput 
                            name="surname"
                            type="text"
                            label="Surname"
                            handleChange={handleChange}
                            value={values.surname}
                        />
                        <button 
                            className="w-full bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
                            onClick={handleSubmit}
                        >
                            Update Profile
                        </button>
                    </div>
                )}
            </Formik>
        </ModalContainer>
    )
}

export default UpdateProfileModal;