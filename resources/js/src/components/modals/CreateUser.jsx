import { Formik } from "formik";
import React from "react";
import ModalContainer from "../general/Modal";
import TextInput from "../general/TextInput";
import { createUser } from "../../services/users.service";

function CreateUserModal({ onClose }) {
    return (
        <ModalContainer
            heading="Create User"
            handleClose={onClose}
        >
            <Formik
                initialValues={{
                    email: "",
                    name: "",
                    surname: "",
                    password: "",
                }}
                onSubmit={async (values) => {
                    const response = await createUser(values);

                    if (response.status === 201) {
                        onClose();
                    }
                }}
            >
                {({ values, handleChange, handleSubmit }) => (
                    <div className="flex flex-col">
                        <TextInput 
                            name="email"
                            type="email"
                            label="Email"
                            handleChange={handleChange}
                            value={values.email}
                        />
                        <TextInput 
                            name="name"
                            type="text"
                            label="Name"
                            handleChange={handleChange}
                            value={values.name}
                        />
                        <TextInput 
                            name="surname"
                            type="text"
                            label="Surname"
                            handleChange={handleChange}
                            value={values.surname}
                        />
                        <TextInput 
                            name="password"
                            type="password"
                            label="Password"
                            handleChange={handleChange}
                            value={values.password}
                        />
                        <button 
                            className="w-full bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
                            onClick={handleSubmit}
                        >
                            Create User
                        </button>
                    </div>
                )}
            </Formik>
        </ModalContainer>
    );
}

export default CreateUserModal;