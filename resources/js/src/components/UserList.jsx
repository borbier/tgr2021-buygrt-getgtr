import React, { useState } from "react";
import { getUserList } from "../services/users.service";
import DeleteUserModal from "./modals/DeleteUserModal";
import CreateUserModal from "./modals/CreateUser";

function UserList() {
    const [deleteUser, setDeleteUser] = useState(false);
    const [createUser, setCreateUser] = useState(false);

    const { data, loading, mutate } = getUserList();

    return (
        <div className="bg-white p-4 m-4 shadow-lg">
            <div className="flex flex-row justify-between items-center">
                <div className="text-lg">
                    User List
                </div>
                <button 
                    className="p-4 m-4 bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded"
                    onClick={() => setCreateUser(!createUser)}
                >
                    Create user
                </button>
            </div>
            {
                !loading &&
                <table>
                <thead>
                    <tr>
                        <th className="px-2 w-1/5">Id</th>
                        <th className="px-2 w-1/5">Email</th>
                        <th className="px-2 w-1/5">Name</th>
                        <th className="px-2 w-2/5"></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map((value) => {
                            if (value.id > 5) {
                                return (
                                    <tr>
                                        <td className="border px-2">{value.id}</td>
                                        <td className="border px-2">{value.name} {value.surname}</td>
                                        <td className="border px-2">{value.email}</td>
                                        <td className="border px-2">
                                        <button 
                                            className="p-4 m-4 bg-red-500 hover:bg-red-700 text-white py-2 px-4 rounded"
                                            onClick={() => setDeleteUser(!deleteUser)}
                                        >
                                            Delete
                                        </button>
                                        </td>
                                        {
                                            deleteUser && <DeleteUserModal 
                                                id={value.id}
                                                onClose={() => {
                                                    mutate();
                                                    setDeleteUser(!deleteUser);
                                                }}
                                            />
                                        }
                                    </tr>
                                );
                            }
                            
                        })
                    }
                </tbody>
                </table>
            }
            {
                createUser && <CreateUserModal 
                    onClose={() => {
                        mutate();
                        setCreateUser(!createUser);
                    }}
                />
            }
        </div>
    );
}

export default UserList;