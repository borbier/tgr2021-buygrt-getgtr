import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';

import Dashboard from './pages/Dashboard';
import User from './pages/User';

function App() {
    return (
        <BrowserRouter>
            <Route exact path="/" component={Dashboard} />
            <Route exact path="/user" component={User} />
        </BrowserRouter>
    );
}

ReactDOM.render(<App />, document.getElementById('app'));
